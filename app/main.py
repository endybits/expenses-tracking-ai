from fastapi import FastAPI
from fastapi import WebSocket
from fastapi.responses import HTMLResponse

from app.utils.ui_interface import UIHtml
from app.utils.gpt_models import financial_gpt, ux_gpt

app = FastAPI()


@app.get("/")
def read_root():
    return HTMLResponse(content=f"{UIHtml}", status_code=200)


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        data = await websocket.receive_text()
        print(data)
        print(type(data))
        await websocket.send_json({"message": "Answer for: " + data,
                                    "sender": "assistant1"})
        financial = await financial_gpt(data)
        print("financial: ", financial)
        await websocket.send_json({"message": financial,
                                    "sender": "assistant1"})