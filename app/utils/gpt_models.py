import asyncio

from openai import OpenAI
from app.conf.config import OPEN_AI_API_KEY


client = OpenAI(api_key=OPEN_AI_API_KEY)

financial_instructions = """Expert helping people with therir expenses and financial health. 
Your task is to keep a conversation with an User expererience professional, expert design products related to financial health. 
"""

ux_designer_instructions = """UX Designer hired to design a product related to financial health.
You will be talking to an expert in financial health.
The main goal is to understand the user needs and design a product that helps them.
Through the conversation, you will be able to understand the user needs and design a product that helps them.
"""

async def financial_gpt(message: str) -> dict:
    messages_financial_expert = [
        {"role": "system", "content": financial_instructions},
        {"role": "user", "content": "I need help tracking my expenses"},
        {"role": "assistant", "content": "Sure, I can help with that. Have you set a monthly budget?"},
        {"role": "user", "content": f"{message}"}, # User message
    ]

    await asyncio.sleep(0.01)
    completion = client.chat.completions.create(
        model = "gpt-4-1106-preview",
        messages= messages_financial_expert,
        # response_format={"type": "json_object"},
    )
    
    generated_text = completion.choices[0].message.content
    return generated_text

async def ux_gpt(message: str) -> str:
    messages_financial_UX_fintech = [
        {"role": "system", "content": ux_designer_instructions},
        {"role": "user", "content": "I need help tracking my expenses"},
        {"role": "assistant", "content": "What kind of expenses do you want to track?"},
        {"role": "user", "content": f"{message}"}, # User message
    ]

    await asyncio.sleep(0.01)
    completion = client.chat.completions.create(
        model = "gpt-4-1106-preview",
        messages= messages_financial_UX_fintech,
        #response_format={ "type": "json_object" }
    )

    generated_text = completion.choices[0].message.content
    return generated_text


