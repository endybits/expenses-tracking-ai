UIHtml = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Expense Tracker Chatbot</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            color: black;
        }

        #navbar {
            background-color: black;
            color: white;
            padding: 10px 0;
        }

        .navbar-brand {
            margin-left: 20px;
            font-size: 1.5em;
        }

        #main {
            display: flex;
            flex-direction: column; /* Corrected property name */
            align-items: center;
            justify-content: center;
            min-height: 80vh;
            padding: 20px 0;
        }

        .chat-container {
            width: 60%;
            max-height: 60vh;
            overflow-y: auto;
            background-color: white;
            border: 2px solid #10a37f;
            padding: 20px;
            border-radius: 10px;
            margin-bottom: 20px;
        }

        /* Styling the scrollbar for chat-container */
        .chat-container::-webkit-scrollbar {
            width: 10px; /* Adjust the width of the scrollbar */
        }

        .chat-container::-webkit-scrollbar-track {
            background: #f1f1f1; /* The track (progress background) */
        }

        .chat-container::-webkit-scrollbar-thumb {
            background: #888; /* Scrollbar handle */
            border-radius: 5px;
        }

        .chat-container::-webkit-scrollbar-thumb:hover {
            background: #555; /* Scrollbar handle on hover */
        }

        .chat-input {
            width: 60%;
            padding: 10px;
            box-shadow: 0 2px 5px rgba(0,0,0,0.2);
            border-radius: 10px;
        }

        #chat-form {
            display: flex;
            justify-content: space-between;
        }

        #message-input {
            flex-grow: 1;
            padding: 10px;
            margin-right: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        #chat-form button {
            padding: 10px 20px;
            background-color: #10a37f;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        #chat-form button:hover {
            background-color: #0e8966; /* Adjusted for darker shade */
        }

        #footer {
            background-color: black;
            color: white;
            text-align: center;
            padding: 10px 0;
            position: fixed;
            bottom: 0;
            width: 100%;
        }

        .container {
            width: 90%;
            margin: auto;
        }

        .chat-messages {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        .chat-message {
            max-width: 80%;
            margin-bottom: 10px;
            padding: 10px;
            border-radius: 10px 10px 0 0px;
            align-self: flex-end;
            margin-right: 10%;
            background-color: #FFF; /* No color for the message box */
            color: black;
            border-bottom: 1px solid #ddd; /* Light gray border for separation */
        }

        .message-header {
            display: flex;
            align-items: center;
            margin-bottom: 5px;
        }

        .message-icon {
            width: 30px;
            height: 30px;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            margin-right: 10px;
            background-color: #DDD; /* Light grey background for icons */
            color: black;
        }

        .user-message .message-icon {
            # background-color: #4A90E2; /* Blue for the user icon */
            color: white;
            background-color: #000000; /* Blue for the user icon */
        }

        .assistant1-message .message-icon {
            # background-color: #7ED321; /* Green for Assistant1 icon */
            background-color: #10A37F; /* Color for Assistant1 icon */
        }

        .assistant2-message .message-icon {
            background-color: #BD10E0; /* Purple for Assistant2 icon */
            background-color: #10A37F; /* Color for Assistant2 icon */
        }

        .message-title {
            font-weight: bold;
        }
    </style>

</head>
<body>
    <!-- Navbar -->
    <nav id="navbar">
        <div class="container">
            <span class="navbar-brand">ExpenseBot</span>
        </div>
    </nav>

    <!-- Main Section -->
    <main id="main">
        <div class="chat-container">
            <!-- Chat messages will be displayed here -->
            <ul class="chat-messages", id="chat-messages">
                <!-- User Message -->
                <li class="chat-message user-message">
                    <div class="message-header">
                        <span class="message-icon">U</span>
                        <span class="message-title">User</span>
                    </div>
                    <p>Hello, I need help tracking my expenses.</p>
                </li>
                
                <!-- Assistant1 Message -->
                <li class="chat-message assistant1-message">
                    <div class="message-header">
                        <span class="message-icon">A1</span>
                        <span class="message-title">Assistant1</span>
                    </div>
                    <p>Assistant1: Sure, I can help with that. Have you set a monthly budget?</p>
                </li>
                
                <!-- Assistant2 Message -->
                <li class="chat-message assistant2-message">
                    <div class="message-header">
                        <span class="message-icon">A2</span>
                        <span class="message-title">Assistant2</span>
                    </div>
                    <p>Assistant2: Also, do you want to track specific categories of expenses?</p>
                </li>
        </div>
        <div class="chat-input">
            <form id="chat-form">
                <input type="text" id="message-input" placeholder="Type your message here..." required>
                <button type="submit">Send</button>
            </form>
        </div>
    </main>

    <!-- Footer -->
    <footer id="footer">
        <div class="container">
            <p>&copy; 2023 ExpenseBot. All rights reserved.</p>
        </div>
    </footer>

    <script>
        // JavaScript code for chatbot interactions will be added here.
        chatMessagesList = document.getElementById('chat-messages');

        // Websocket connection
        let ws = new WebSocket("ws://localhost:8000/ws");
        ws.onmessage = function(event) {
            console.log(event.data);
            const message = JSON.parse(event.data);
        };

        // Chatbot interactions
        const chatForm = document.getElementById('chat-form');
        chatForm.addEventListener('submit', (e) => {
            e.preventDefault();
            const messageInput = document.getElementById('message-input');
            const message = messageInput.value;
            messageInput.value = '';
            console.log("Sending message: " + message);
            ws.send(message);

            // Create a new list item for the user message
            let userMessage = document.createElement('li')
			userMessage.setAttribute('class', 'chat-message user-message')
            userMessage.innerHTML = `
                <div class="message-header">
                    <span class="message-icon">U</span>
                    <span class="message-title">User</span>
                </div>
                <p>${message}</p>
            `;
            chatMessagesList.appendChild(userMessage);
        });
    </script>
</body>
</html>
"""