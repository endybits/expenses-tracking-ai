import os
import json

path_file = "app/conf/config.json"

def get_config() -> dict:
    if not os.path.exists(path_file):
        raise Exception("File config.json not found")
    
    return json.loads(open(path_file, "r", encoding="UTF-8").read())

def get_config_by_key(key):
    return get_config().get(key, None)

OPEN_AI_API_KEY = get_config_by_key("openai_api_key")