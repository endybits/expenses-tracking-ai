make install:
	pip install -U pip && \
	pip install -r requirements.txt

make format:
	black .

make test:
	pytest

make launch:
	uvicorn app.main:app --reload