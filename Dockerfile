# Use the Amazon Linux 2023 as the base image
FROM amazonlinux:2023

# Set the working directory
WORKDIR /code

# Install any dependencies or packages required for your application
RUN yum install -y python3.11 python3-pip make

# Copy the necessary files to the working directory
COPY ./requirements.txt /code/requirements.txt
COPY ./makefile /code/makefile

# Install the dependencies
RUN make install

# Copy the rest of the files to the working directory
COPY ./app /code/app

# Expose the port that your application will be running on
EXPOSE 8000

# Set the entry point or command to run your fastapi application
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]